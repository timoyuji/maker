package ldh.database.util;

public class JavaMapJdbc {
	
	private Class<?> clazz;
	private String jdbcType;
	
	public JavaMapJdbc(String jdbcType, Class<?> clazz) {
		this.jdbcType = jdbcType;
		this.clazz = clazz;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public String getJdbcType() {
		return jdbcType;
	}

	public void setJdbcType(String jdbcType) {
		this.jdbcType = jdbcType;
	}
}
