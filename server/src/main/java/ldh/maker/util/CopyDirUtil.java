package ldh.maker.util;

import java.io.*;
import java.nio.file.*;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static java.nio.file.StandardCopyOption.*;

/**
 * Created by ldh on 2017/4/8.
 */
public class CopyDirUtil {

    public static void copyResourceDir(String srcDir, String root, List<String> targetDir) throws IOException {
        String target = makePath(root, targetDir);
        if (!srcDir.toLowerCase().contains("jar")) {
            File file = new File(srcDir);
            if (file.isFile()) {
                File targetFile = new File(target);
                if (targetFile.exists()) return;
                Files.copy(Paths.get(srcDir), Paths.get(target), REPLACE_EXISTING);
                return;
            }
            copyDir(file, target);
            return;
        }

        copyRecourseFromJarByFolder(srcDir, target);
    }

    public static void copyDir(File file, String target) throws IOException {
        File[] files = file.listFiles();
        target = makePath(target, Arrays.asList(file.getName()));
        for (File f : files) {
            String t = target + "/" + f.getName();
            if (f.isFile()) {
                File targetFile = new File(t);
                if (targetFile.exists()) return;
                Files.copy(Paths.get(f.toURI()), Paths.get(t), REPLACE_EXISTING);
                continue;
            }
            copyDir(f, target);
        }
    }

    public static void copyFile(String file, String root, List<String> targetDir) throws IOException {
        String target = makePath(root, targetDir);
        File f = new File(file);
//        target = makePath(target, Arrays.asList(f.getName()));
        String t = target + "/" + f.getName();
        Files.copy(Paths.get(f.toURI()), Paths.get(t), REPLACE_EXISTING);
    }

    public static String makePath(String path, List<String> dirs) {
        for (String s : dirs) {
            path += "/" + s;
            File file = new File(path);
            while (!file.exists()) {
                file.mkdir();
            }
        }
        return path;
    }

    public static String makePath(String path, String pack) {
        String[] strs = pack.split("/");
        for (String s : strs) {
            path += "/" + s;
            File file = new File(path);
            while (!file.exists()) {
                file.mkdir();
            }
        }
        return path;
    }

    public static void copyRecourseFromJarByFolder(String folderPath, String targetDir) throws IOException {
        int index = folderPath.indexOf("jar");
        String str = folderPath.substring(0, index);
        str = str.substring(str.lastIndexOf("/")+1);
        String jar = str + "jar";
        JarFile jarFile = new JarFile(jar);
        String pack = folderPath.substring(folderPath.indexOf("!")+2);
        copyJarResources(jarFile, pack, targetDir);
        jarFile.close();
    }

    private static void copyJarResources(JarFile jarFile, String pack, String targetDir) throws IOException{
        Enumeration<JarEntry> enums = jarFile.entries();
        while (enums.hasMoreElements()) {
            JarEntry entry = enums.nextElement();
            if (entry.getName().startsWith(pack) && !entry.isDirectory()) {
                copyRecourseFromJar(jarFile, entry, targetDir);
            }
        }
    }

    public static void copyRecourseFromJar(JarFile jarFile, JarEntry entry, String targetDir) throws IOException {
        String srcFile = entry.getName();
        String srcDir = srcFile.substring(0, srcFile.lastIndexOf("/"));
        String td = makePath(targetDir, srcDir);
        InputStream in = jarFile.getInputStream(entry);
        Files.copy(in, Paths.get(targetDir + "/" + srcFile), REPLACE_EXISTING);
    }
}
