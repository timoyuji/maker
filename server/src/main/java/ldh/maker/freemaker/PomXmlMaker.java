package ldh.maker.freemaker;

/**
 * Created by ldh on 2017/4/8.
 */
public class PomXmlMaker extends FreeMarkerMaker<PomXmlMaker> {

    protected String projectRootPackage;
    protected String project;

    public PomXmlMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    public PomXmlMaker project(String project) {
        this.project = project;
        return this;
    }

    @Override
    public PomXmlMaker make() {
        data();
        if (ftl == null) {
            this.out("pom.ftl", data);
        } else {
            this.out(ftl, data);
        }
        return this;
    }

    @Override
    public void data() {
        fileName = "pom.xml";
        data.put("projectRootPackage", projectRootPackage);
        data.put("project", project);
    }
}
