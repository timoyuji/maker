package ldh.maker.freemaker;

import ldh.bean.util.BeanInfoUtil;
import ldh.common.PageResult;
import ldh.common.json.ValuedEnumObjectSerializer;
import ldh.database.Column;
import ldh.database.UniqueIndex;
import ldh.maker.util.FreeMakerUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class ServiceMaker extends BeanMaker<ServiceMaker> {
	
	private Class<?> bean;
	private BeanMaker<?> beanWhereMaker;
	private BeanMaker<?> daoMaker;
	private String service;
	
	public ServiceMaker() {
		this.imports(List.class)
		    .imports(ArrayList.class)
		    .imports(PageResult.class)
		    .imports(Resource.class)
		    .imports(Transactional.class)
		    .imports(Service.class)
		    ;
	}
	
	public ServiceMaker service(String service) {
		this.service = service;
		return this;
	}
	
	public ServiceMaker daoMaker(BeanMaker<?> daoMaker) {
		this.daoMaker = daoMaker;
		imports.add(daoMaker.getName());
		return this;
	}
	
	public ServiceMaker bean(Class<?> bean) {
		this.bean = bean;
		this.imports.add(bean.getName());
		return this;
	}
	
	public ServiceMaker beanWhereMaker(BeanMaker<?> beanWhereMaker) {
		this.beanWhereMaker = beanWhereMaker;
		this.imports.add(beanWhereMaker.getName());
		return this;
	}
	
	public void data() {
		check();
		if (bean != null) {
			data.put("bean", bean.getSimpleName());
			data.put("beanWhere", bean.getSimpleName() + "Where");
		}
		if (beanWhereMaker != null){
			data.put("bean", beanWhereMaker.getSimpleName());
			data.put("beanWhere", beanWhereMaker.getSimpleName() + "Where");
		}
		Set<UniqueIndex> uis = this.table.getIndexies();
		if (uis != null) {
			for (UniqueIndex ui : uis) {
				for (Column c : ui.getColumns()) {
					if (!BeanInfoUtil.isBaseClass(c.getPropertyClass())) {
						this.imports(c.getPropertyClass());
					}
				}
			}
		}

		String root = pack.substring(0, pack.lastIndexOf("."));
		for (Column column : table.getColumnList()) {
			if (FreeMakerUtil.isEnum(column)) {
				imports.add(root + ".constant." + column.getJavaType());
				imports.add(ValuedEnumObjectSerializer.class.getName());
			}
		}

		if (service == null) {
			service = FreeMakerUtil.javaName(data.get("bean").toString() + "Service");
		}
		data.put("service", service);
		if (extendsClassName != null) {
			this.implementList.add(extendsClassName);
		}
		data.put("table", table);
		data.put("daoMaker", daoMaker);
		super.data();
	}
	
	public void check() {
		if (bean == null && beanWhereMaker == null) {
			throw new NullPointerException("bean or beanMaker is not null!!!");
		}
		if (daoMaker == null) {
			throw new NullPointerException("daoMaker is not null!!!");
		}
		super.check();
	}
	
	@Override
	public ServiceMaker make() {
		data();
		out("service.ftl", data);
		
		return this;
	}
	
	public static void main(String[] args) {
		String outPath = "E:\\project\\eclipse\\datacenter\\website_statistics\\admin\\src\\main\\base\\ldh\\base\\make\\freemaker";
		
		new ServiceMaker()
		 	.pack("ldh.base.make.freemaker")
		 	.outPath(outPath)
		 	.className("ScenicDao")
		 	.make();
		
	}

	
}
