package ldh.maker.freemaker;

import ldh.database.Table;
import ldh.maker.database.TableInfo;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxMaker extends BeanMaker<JavafxMaker> {

    protected TableInfo tableInfo;
    protected String projectPackage;
    protected String fileName;

    public JavafxMaker tableInfo(TableInfo tableInfo) {
        this.tableInfo = tableInfo;
        return this;
    }

    public JavafxMaker projectPackage(String projectPackage) {
        this.projectPackage = projectPackage;
        return this;
    }

    @Override
    public JavafxMaker make() {
        data();
        out(ftl, data);

        return this;
    }

    @Override
    public void data() {
        data.put("tableInfo", tableInfo);
        data.put("table", table);
        data.put("projectPackage", projectPackage);
        data.put("controllerPackage", pack);
    }
}
