package ${beanPackage};

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ldh on 2018/6/29.
 */
public class BeanUtil {

    private static Map<Class, Object> BEAN_INSTANCE = new HashMap<>();

    public static <T>T getBean(Class<T> clazz) {
        if (BEAN_INSTANCE.containsKey(clazz)) {
            return (T) BEAN_INSTANCE.get(clazz);
        }
        Object bean = createBean(clazz);
        if (bean != null) {
            BEAN_INSTANCE.putIfAbsent(clazz, bean);
        }
        return (T) BEAN_INSTANCE.get(clazz);
    }

    public static Object createBean(Class clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

}
