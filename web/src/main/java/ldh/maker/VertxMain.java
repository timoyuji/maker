package ldh.maker;

import ldh.maker.component.VertxContentUiFactory;
import ldh.maker.util.UiUtil;

/**
 * Created by ldh123 on 2018/6/23.
 */
public class VertxMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new VertxContentUiFactory());
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}
