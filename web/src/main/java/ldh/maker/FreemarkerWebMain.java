package ldh.maker;

import ldh.maker.component.BootstrapContentUiFactory;
import ldh.maker.component.FreemarkerContentUiFactory;
import ldh.maker.util.UiUtil;

public class FreemarkerWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new FreemarkerContentUiFactory());
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}

